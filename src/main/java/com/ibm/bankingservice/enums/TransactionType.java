package com.ibm.bankingservice.enums;

public enum TransactionType {
	deposit, withdraw;
}

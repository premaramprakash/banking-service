package com.ibm.bankingservice.exception;


public class BusinessException extends RuntimeException {

  /**
   * Constructs a new ResourceNotFoundException object with a detail message
   * 
   * @param message - the detail message
   */
  public BusinessException(String message) {
    super(message);
  }

  /**
   * Constructs a new ResourceNotFoundException object with a detail message and throwable cause.
   * 
   * @param message - the detail message
   * @param cause - the cause (which is saved for later retrieval by the {@link #getCause()} method). (A null value is
   *        permitted, and indicates that the cause is nonexistent or unknown.)
   */
  public BusinessException(String message, Throwable cause) {
    super(message, cause);
  }

}
package com.ibm.bankingservice.web;

import com.ibm.bankingservice.dto.TransactionDTO;
import com.ibm.bankingservice.enums.TransactionType;
import com.ibm.bankingservice.infra.LogMethodParam;
import com.ibm.bankingservice.kafka.TransactionEvent;
import com.ibm.bankingservice.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	@GetMapping("{account-number}/balance")
	@LogMethodParam
	public ResponseEntity<Double> getBalance(@PathVariable(value = "account-number") String accountNumber) {
		return ResponseEntity.of(transactionService.getBalance(accountNumber));
	}

	@GetMapping("{account-number}/summary")
	@LogMethodParam
	public ResponseEntity<List<TransactionDTO>> getSummary(@PathVariable(value = "account-number") String accountNumber,
	                                                       @RequestParam(name = "fromdate", required = false) Instant fromDate,
	                                                       @RequestParam(name = "todate", required = false) Instant toDate
	) {
		return ResponseEntity.of(transactionService.getSummary(accountNumber, fromDate, toDate));
	}

	@GetMapping("{account-number}/summary/{type}")
	@LogMethodParam
	public ResponseEntity<List<TransactionDTO>> getSummaryByType(@PathVariable(value = "account-number") String accountNumber,
	                                                             @PathVariable(value = "type") TransactionType type,
	                                                             @RequestParam(name = "fromdate", required = false) Instant fromDate,
	                                                             @RequestParam(name = "todate", required = false) Instant toDate) {
		return ResponseEntity.of(transactionService.getSummary(accountNumber, type, fromDate, toDate));
	}

	@PostMapping("load-data")
	@LogMethodParam
	public ResponseEntity<Void> loadData(@RequestBody TransactionEvent transactionEvent) {
		transactionService.loadData(transactionEvent);
		return ResponseEntity.ok().build();
	}
}

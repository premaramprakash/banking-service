package com.ibm.bankingservice.kafka;

import com.ibm.bankingservice.infra.LogMethodParam;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);
    
    @Value("${transactions.topic}")
    private String topic;

    @Autowired
    private KafkaTemplate<String, TransactionEvent> kafkaTemplate;

	@LogMethodParam
    public void send(TransactionEvent payload) {
        LOGGER.info("sending payload='{}' to topic='{}'", payload, topic);
        try{
	        kafkaTemplate.send(topic, payload);
        }catch(Exception e){
        	LOGGER.error("Publish failed ", e);
        }
        
    }
}

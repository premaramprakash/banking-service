package com.ibm.bankingservice.kafka;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ibm.bankingservice.enums.TransactionType;
import com.ibm.bankingservice.infra.InstantDeSerializer;
import com.ibm.bankingservice.infra.InstantSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class TransactionEvent {

    private String accountNumber;

	@JsonSerialize(using = InstantSerializer.class)
	@JsonDeserialize(using = InstantDeSerializer.class)
    private Instant transactionDate;

    private TransactionType transactionType;

    private Double amount;
}

package com.ibm.bankingservice.infra;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.time.Instant;

public class InstantDeSerializer extends JsonDeserializer<Instant> {
  /**
   * Instant Deserializer
   * 
   * @param arg0
   * @param arg1
   * @return Instant
   */
  @Override
  public Instant deserialize(JsonParser arg0, DeserializationContext arg1) throws IOException {

    return Instant.parse(arg0.getText());
  }
}
package com.ibm.bankingservice.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ibm.bankingservice.enums.TransactionType;
import com.ibm.bankingservice.infra.InstantDeSerializer;
import com.ibm.bankingservice.infra.InstantSerializer;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="transaction_id")
    private String transactionId;

    @Column(name="account_number")
    private String accountNumber;

	@JsonSerialize(using = InstantSerializer.class)
	@JsonDeserialize(using = InstantDeSerializer.class)
	@Column(name="transaction_date")
	private Instant transactionDate;

    @Column(name="transaction_type")
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @Column(name="amount")
    private Double amount;
    
    @Column(name="balance")
    private Double balance;
}

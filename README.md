# Banking Service

## Application Flows
* There are 2 flows.
* Input - Kafka Listener
* Output - get endpoints

### Kafka listener
* We have a kafka consumer who listens when transaction event happens and consumes the data from 'transaction-created-events' topic and creates the transaction record in the database.
* to mimic Kafka Event Produce, use http://localhost:8080/transactions/load-data

### Exposed endpoints
* This flow is to expose three api's to get the transaction data from the database.
* For getting the account balance:- http://localhost:8080/transactions/{account-number}/balance
* For getting the summary of the application:-
  http://localhost:8080/transactions/{account-number}/summary
  (date range parameters are optional)
* For getting the summaryWithType of the application:-
  http://localhost:8080/transactions/{account-number}/summary/{type}
  (date range parameters are optional)

## NFR
* java - 8
* Springboot - 2.4.2
* Kafka - 2.7.0
* h2 database - 1.4.2
* actuator
* swagger documentation
* Exception handling
* automatic method entry and parameter using Custom annotations
* used pessimistic lock to avoid the double balance fetch issue


## local setup
* setup Java and maven
* setup intellij
* clone this project
* download kafka
* Start zookeeper:
  `bin\windows\zookeeper-server-start.bat config\zookeeper.properties`

* Start Kafka server:
  `bin\windows\kafka-server-start.bat config\server.properties`

* Create topic:
  `bin\windows\kafka-topics.bat --create --topic transaction-created-events --bootstrap-server localhost:9092`

* to explore the database use application.yml details and url :) http://localhost:8080/h2-console

* hit swagger and explore the app :) http://localhost:8080/swagger-ui.html

## References
* [Spring initializer](https://start.spring.io/)
* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Kafka quick start](https://kafka.apache.org/quickstart)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#boot-features-developing-web-applications)